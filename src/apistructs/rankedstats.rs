use serde::Deserialize;

//TODO: Implement wrapper methods

#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct RankedStats {
    name: String,
    brawlhalla_id: u64,
    rating: u64,
    peak_rating: u64,
    tier: String,
    wins: u64,
    games: u64,
    region: String,
    global_rank: u64,
    region_rank: u64,
    legends: Vec<RankedLegend>,
    #[serde(flatten)]
    _2v2: std::collections::HashMap<String, Vec<RankedDuo>>,
}

#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct RankedLegend {
    legend_id: u64,
    legend_name_key: String,
    rating: u64,
    peak_rating: u64,
    tier: String,
    wins: u64,
    games: u64,
}

#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct RankedDuo {
    brawlhalla_id_one: u64,
    brawlhalla_id_two: u64,
    rating: u64,
    peak_rating: u64,
    tier: String,
    wins: u64,
    games: u64,
    teamname: String,
    region: u64,
    global_rank: u64,
}
