use serde::Deserialize;
use std::convert::TryInto;

#[derive(Deserialize, Debug)]
pub struct Clan {
    clan_id: u64,
    clan_name: String,
    clan_create_date: u64,
    clan_xp: String,
    clan: Vec<ClanMember>,
}

impl Clan {
    pub fn clan_id(&self) -> u64 {
        self.clan_id
    }

    pub fn clan_name(&self) -> &str {
        &self.clan_name
    }

    pub fn clan_create_date(&self) -> chrono::NaiveDateTime {
        chrono::NaiveDateTime::from_timestamp(self.clan_create_date.try_into().unwrap(), 0)
    }

    pub fn clan_xp(&self) -> u128 {
        (&self.clan_xp).parse().unwrap()
    }

    pub fn clan_members(&self) -> &Vec<ClanMember> {
        &self.clan
    }
}

#[derive(Deserialize, Debug)]
pub struct ClanMember {
    brawlhalla_id: u64,
    name: String,
    rank: String,
    join_date: u64,
    xp: u64,
}

impl ClanMember {
    pub fn brawlhalla_id(&self) -> u64 {
        self.brawlhalla_id
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn rank(&self) -> &str {
        &self.rank
    }

    pub fn join_date(&self) -> chrono::NaiveDateTime {
        chrono::NaiveDateTime::from_timestamp(self.join_date.try_into().unwrap(), 0)
    }

    pub fn xp(&self) -> u64 {
        self.xp
    }
}
