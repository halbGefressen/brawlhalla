use serde::Deserialize;

/// Represents the JSON error response that gets sent by the API server on a false API call.
/// Calling errors are mostly caused by the rate limit or invalid API keys.
#[derive(Deserialize, Debug, PartialEq)]
pub struct CallingError {
    error: InnerError,
}

impl CallingError {
    /// Returns the HTTP error code.
    pub fn code(&self) -> u64 {
        self.error.code
    }

    /// Returns the error message.
    pub fn message(&self) -> &str {
        &self.error.message
    }
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct InnerError {
    code: u64,
    message: String,
}

/// This enum represents any error that could occur during an API call.
/// It can either be a ReqwestError, which occurs when there is a problem with the network or the API,
/// or a CallingError, which represents the JSON error response that gets sent by the API server on a false API call.
/// Calling errors are mostly caused by the rate limit or invalid API keys.
#[derive(Debug)]
pub enum APIError {
    ReqwestError(reqwest::Error),
    CallingError(CallingError),
}

impl From<reqwest::Error> for APIError {
    fn from(a: reqwest::Error) -> Self {
        Self::ReqwestError(a)
    }
}
