pub mod clan;
pub mod error;
pub mod legends;
pub mod player;
pub mod rankedstats;
pub mod rankings;
pub mod result;
pub mod searchresult;

pub use clan::Clan;
pub use error::{APIError, CallingError};
pub use legends::*;
pub use player::Player;
pub use rankedstats::RankedStats;
pub use rankings::Rankings;
pub use searchresult::*;
