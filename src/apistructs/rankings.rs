use serde::Deserialize;

//TODO: Implement wrapper methods

#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct Rankings {
    rank: u64,
    name: String,
    brawlhalla_id: u64,
    best_legend: u64,
    best_legend_games: u64,
    best_legend_wins: u64,
    rating: u64,
    tier: String,
    games: u64,
    wins: u64,
    region: String,
    peak_rating: u64,
}
