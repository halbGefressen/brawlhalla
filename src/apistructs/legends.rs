use serde::Deserialize;
/// Represents an entry of the APIConnection.legends() result. Methods are self-explanatory.
#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct SmallLegend {
    legend_id: u64,
    legend_name_key: String,
    bio_name: String,
    bio_aka: String,
    weapon_one: String,
    weapon_two: String,
    strength: String,
    dexterity: String,
    defense: String,
    speed: String,
}

#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct FullLegend {
    legend_id: u64,
    legend_name_key: String,
    bio_name: String,
    bio_aka: String,
    bio_quote: String,
    bio_quote_about_attrib: String,
    bio_quote_from: String,
    bio_quote_from_attrib: String,
    bio_text: String,
    bot_name: String,
    weapon_one: String,
    weapon_two: String,
    strength: String,
    dexterity: String,
    defense: String,
    speed: String,
}
