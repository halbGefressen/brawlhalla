use serde::Deserialize;
//TODO: Implement wrapper methods

#[derive(Debug, Deserialize, ShortHand)]
#[shorthand(disable(set))]
pub struct Player {
    brawlhalla_id: u64,
    name: String,
    xp: u64,
    level: u64,
    xp_percentage: f64,
    games: u64,
    wins: u64,
    damagebomb: String,
    damagemine: String,
    damagespikeball: String,
    damagesidekick: String,
    hitsnowball: u64,
    kobomb: u64,
    komine: u64,
    kospikeball: u64,
    kosidekick: u64,
    kosnowball: u64,
    legends: Vec<PlayerLegend>,
    clan: PlayerClan,
}

#[derive(Deserialize, Debug, ShortHand)]
#[shorthand(disable(set))]
pub struct PlayerLegend {
    legend_id: u64,
    legend_name_key: String,
    damagedealt: String,
    damagetaken: String,
    kos: u32,
    falls: u32,
    suicides: u32,
    teamkos: u32,
    matchtime: u64,
    games: u32,
    wins: u32,
    damageunarmed: String,
    damagethrownitem: String,
    damageweaponone: String,
    damageweapontwo: String,
    damagegadgets: String,
    kounarmed: u32,
    kothrownitem: u32,
    koweaponone: u32,
    koweapontwo: u32,
    kogadgets: u32,
    timeheldweaponone: u64,
    timeheldweapontwo: u64,
    xp: u64,
    level: u8,
    xp_percentage: f64,
}

#[derive(Deserialize, Debug, ShortHand)]
#[shorthand(disable(set))]
pub struct PlayerClan {
    clan_name: String,
    clan_id: u64,
    clan_xp: String,
    personal_xp: u64,
}
