use serde::Deserialize;

#[derive(Deserialize, Debug, ShortHand)]
#[shorthand(disable(set))]
pub struct SearchResult {
    brawlhalla_id: u64,
    name: String,
}
