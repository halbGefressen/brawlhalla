#[derive(serde::Deserialize, Debug)]
#[serde(untagged)]
pub enum APIResult<T> {
    Ok(T),
    Err(crate::apistructs::CallingError),
}
