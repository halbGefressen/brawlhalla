extern crate brawlhalla;

use brawlhalla::*;

// CONFIGURE TESTS
const APIKEY: &'static str = "";
const STEAMID: u64 = 42069; 
// END CONFIGURE TESTS

#[tokio::test]
async fn test_search() {
    ConnectionBuilder::default()
        .with_apikey(APIKEY)
        .build()
        .search_by_steam_id(STEAMID)
        .await
        .unwrap();
}

#[tokio::test]
async fn test_rankings() {
    ConnectionBuilder::default()
        .with_apikey(APIKEY)
        .build()
        .rankings("1v1", "eu", 1)
        .await
        .unwrap();
}

#[tokio::test]
async fn test_rankings_search() {
    ConnectionBuilder::default()
        .with_apikey(APIKEY)
        .build()
        .rankings_search("1v1", "eu", "halbGefressen")
        .await
        .unwrap();
}

#[tokio::test]
async fn test_player_search() {
    let conn = ConnectionBuilder::default().with_apikey(APIKEY).build();
    conn.player_stats(
        conn.search_by_steam_id(STEAMID)
            .await
            .unwrap()
            .brawlhalla_id(),
    )
    .await
    .unwrap();
}

#[tokio::test]
async fn test_ranked() {
    let conn = ConnectionBuilder::default().with_apikey(APIKEY).build();
    conn.ranked_stats(
        conn.search_by_steam_id(STEAMID)
            .await
            .unwrap()
            .brawlhalla_id(),
    )
    .await
    .unwrap();
}

#[tokio::test]
async fn test_clan() {
    let conn = ConnectionBuilder::default().with_apikey(APIKEY).build();
    conn.claninfo(1).await.unwrap();
}

#[tokio::test]
async fn test_legends() {
    let conn = ConnectionBuilder::default().with_apikey(APIKEY).build();
    conn.legends().await.unwrap();
}

#[tokio::test]
async fn test_legendinfo() {
    let conn = ConnectionBuilder::default().with_apikey(APIKEY).build();
    conn.legendinfo(3).await.unwrap();
}

#[tokio::test]
async fn test_error() {
    println!(
        "{:?}",
        ConnectionBuilder::default()
            .with_apikey("invalid_key")
            .build()
            .legends()
            .await
            .unwrap_err()
    );
}
